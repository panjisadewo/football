package panji.com.kotlinscheduledicoding

import io.reactivex.Flowable
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import panji.com.kotlinscheduledicoding.entity.lastmatch.LastMatchPresenter
import panji.com.kotlinscheduledicoding.entity.lastmatch.LastMatchView
import panji.com.kotlinscheduledicoding.model.matchevent.MatchEvent
import panji.com.kotlinscheduledicoding.model.matchevent.MatchEventPresenter
import panji.com.kotlinscheduledicoding.model.matchevent.MatchEventResponse

import io.reactivex.disposables.CompositeDisposable


import org.mockito.Mockito.verify
import panji.com.kotlinscheduledicoding.utils.*

class LastMatchPresenterTesting {

    @Mock
    lateinit var mView: LastMatchView.View

    @Mock
    lateinit var matchRepositoryImpl: MatchEventPresenter

    @Mock
    lateinit var scheduler: SchedulerProviderView

    @Mock
    lateinit var mPresenter: LastMatchPresenter

    @Mock
    lateinit var match: MatchEventResponse

    @Mock
    lateinit var footballMatch: Flowable<MatchEventResponse>

    private val event = mutableListOf<MatchEvent>()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        scheduler = TrampolineSchedulerProvider()
        match = MatchEventResponse(event)
        footballMatch = Flowable.just(match)
        mPresenter = LastMatchPresenter(mView, matchRepositoryImpl, scheduler)
        `when`(matchRepositoryImpl.getFootballMatch("4332")).thenReturn(footballMatch)
    }

    @Test
    fun getFootballData() {
        mPresenter.getFootballData()
        verify(mView).showLoading()
        verify(mView).displayFootballMatch(event)
        verify(mView).hideLoading()
    }
}