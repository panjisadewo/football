package panji.com.kotlinscheduledicoding

import org.junit.Test

import org.junit.Assert.*
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import panji.com.kotlinscheduledicoding.api.TheSportDBApi

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ApiMatchTest {
    @Test
    fun testDoRequest() {
        val apiMatch = mock(TheSportDBApi::class.java)
        val url = "https://www.thesportsdb.com/api/v1/json/1/search_all_teams.php?l=English%20Premier%20League"
        apiMatch.doRequest(url)
        verify(apiMatch).doRequest(url)
    }

    @Test
    fun testApiMatchAllLeague() {
        val apiMatch = mock(TheSportDBApi::class.java)
        val url = "https://www.thesportsdb.com/api/v1/json/1/all_leagues.php"
        apiMatch.doRequest(url)
        verify(apiMatch).doRequest(url)
    }
}
