package panji.com.kotlinscheduledicoding.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_team.view.*
import org.jetbrains.anko.startActivity
import panji.com.kotlinscheduledicoding.R
import panji.com.kotlinscheduledicoding.entity.teamdetail.TeamDetailActivity
import panji.com.kotlinscheduledicoding.model.teams.Teams

class TeamDuaAdapter(val teamList: List<Teams>, val context: Context?): RecyclerView.Adapter<TeamDuaAdapter.TeamViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeamViewHolder {
        return TeamViewHolder(LayoutInflater.from(context).inflate(R.layout.item_team, parent, false))
    }

    override fun getItemCount() = teamList.size

    override fun onBindViewHolder(holder: TeamViewHolder, position: Int) {
        holder.bind(teamList[position])
    }


    inner class TeamViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        fun bind(team: Teams){
            itemView.tvTeam.text = team.strTeam
            Glide.with(itemView.context)
                    .load(team.strTeamBadge)
                    .apply(RequestOptions().placeholder(R.drawable.navigation_empty_icon))
                    .into(itemView.imgTeam)

            itemView.setOnClickListener {
                itemView.context.startActivity<TeamDetailActivity>("team" to team)
            }
        }

    }
}