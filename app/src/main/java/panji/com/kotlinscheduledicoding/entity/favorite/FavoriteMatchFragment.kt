package panji.com.kotlinscheduledicoding.entity.favorite

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.disposables.CompositeDisposable
import panji.com.kotlinscheduledicoding.R
import panji.com.kotlinscheduledicoding.adapter.TeamsAdapter
import panji.com.kotlinscheduledicoding.api.TheSportDBApi
import panji.com.kotlinscheduledicoding.api.TheSportDBRest
import panji.com.kotlinscheduledicoding.model.matchevent.MatchEvent
import panji.com.kotlinscheduledicoding.model.matchevent.MatchEventPresenter
import panji.com.kotlinscheduledicoding.model.repo.RepositoryPresenter
import panji.com.kotlinscheduledicoding.utils.AppSchedulerProvider
import panji.com.kotlinscheduledicoding.utils.hide
import panji.com.kotlinscheduledicoding.utils.show

import kotlinx.android.synthetic.main.fragment_favorite_match.*
import org.jetbrains.anko.support.v4.onRefresh

class FavoriteMatchFragment : Fragment(), FavoriteMatchView.View {
    override fun hideLoading() {
        mainProgressBarFav.hide()
        rvFootballFav.visibility = View.VISIBLE
    }

    override fun showLoading() {
        mainProgressBarFav.show()
        rvFootballFav.visibility = View.INVISIBLE
    }


    private var matchLists: MutableList<MatchEvent> = mutableListOf()
    private lateinit var mPresenter: FavoriteMatchPresenter


    override fun displayFootballMatch(matchList: List<MatchEvent>) {
        matchLists.clear()
        matchLists.addAll(matchList)
        val layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        rvFootballFav.layoutManager = layoutManager
        rvFootballFav.adapter = TeamsAdapter(matchList, context)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val service = TheSportDBApi.getClient().create(TheSportDBRest::class.java)
        val request = MatchEventPresenter(service)
        val repository = RepositoryPresenter(context!!)
        val appSchedulerProvider = AppSchedulerProvider()
        mPresenter = FavoriteMatchPresenter(this, request, repository, appSchedulerProvider)
        mPresenter.getFootballMatchData()
        swipe_refresh_layout_fav.onRefresh {
            mPresenter.getFootballMatchData()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_favorite_match, container, false)
    }

    override fun hideSwipeRefresh() {
        swipe_refresh_layout_fav.isRefreshing = false
        mainProgressBarFav.hide()
        rvFootballFav.visibility = View.VISIBLE
    }

    override fun onDestroyView() {
        mPresenter.destroy()
        super.onDestroyView()
    }
}