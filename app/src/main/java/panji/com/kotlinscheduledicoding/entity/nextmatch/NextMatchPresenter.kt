package panji.com.kotlinscheduledicoding.entity.nextmatch

import panji.com.kotlinscheduledicoding.model.matchevent.MatchEventPresenter
import panji.com.kotlinscheduledicoding.utils.SchedulerProviderView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers


class NextMatchPresenter(private val mView: NextMatchView.View, private val matchEventPresenter: MatchEventPresenter, private val schedulers: SchedulerProviderView) : NextMatchView.Presenter {

    private val compositeDisposable = CompositeDisposable()

    override fun getFootballUpcomingData() {
        mView.showLoading()
        compositeDisposable.add(matchEventPresenter.getUpcomingMatch("4332")
                .observeOn(schedulers.ui())
                .subscribeOn(schedulers.io())
                .subscribe {
                    mView.displayFootballMatch(it.events)
                    mView.hideLoading()
                })
    }

    fun destroy() {
        compositeDisposable.dispose()
    }
}