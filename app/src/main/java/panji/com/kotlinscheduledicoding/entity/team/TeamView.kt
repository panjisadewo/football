package panji.com.kotlinscheduledicoding.entity.team

import panji.com.kotlinscheduledicoding.model.teams.Teams

interface TeamView {
    interface View{
        fun displayTeams(teamList: List<Teams>)
        fun hideLoading()
        fun showLoading()

    }
    interface Presenter{
        fun getTeamData(leagueName: String)
        fun searchTeam(teamName: String)
        fun onDestroy()
    }
}