package panji.com.kotlinscheduledicoding.entity.team

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.fragment_team_over.*
import panji.com.kotlinscheduledicoding.R
import panji.com.kotlinscheduledicoding.model.teams.Teams

class TeamOverFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_team_over, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val team: Teams? = arguments?.getParcelable("teams")
        initView(team)
    }

    fun initView(teamInfo: Teams?){
        Glide.with(this)
                .load(teamInfo?.strTeamBadge)
                .apply(RequestOptions().placeholder(R.drawable.navigation_empty_icon))
                .into(imgBadge)

        teamName.text = teamInfo?.strTeam
        tvManager.text = teamInfo?.strManager
        tvStadium.text = teamInfo?.strStadium
        teamOverview.text = teamInfo?.strDescriptionEN
    }


}
