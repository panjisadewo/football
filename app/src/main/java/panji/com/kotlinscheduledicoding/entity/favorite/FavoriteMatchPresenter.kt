package panji.com.kotlinscheduledicoding.entity.favorite

import panji.com.kotlinscheduledicoding.R
import panji.com.kotlinscheduledicoding.R.id.*
import panji.com.kotlinscheduledicoding.model.matchevent.MatchEvent
import panji.com.kotlinscheduledicoding.model.matchevent.MatchEventPresenter
import panji.com.kotlinscheduledicoding.model.repo.RepositoryPresenter
import panji.com.kotlinscheduledicoding.utils.AppSchedulerProvider
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class FavoriteMatchPresenter(private val mView: FavoriteMatchView.View,
                             private val matchPresenter: MatchEventPresenter,
                             private val repositoryPresenter: RepositoryPresenter,
                             private val appSchedulerProvider: AppSchedulerProvider) : FavoriteMatchView.Presenter {

    private val compositeDisposable = CompositeDisposable()

    override fun getFootballMatchData() {
        mView.showLoading()
        val favoriteList = repositoryPresenter.getMatchFromDb()

        val eventList: MutableList<MatchEvent> = mutableListOf()
        for (fav in favoriteList) {
            compositeDisposable.add(matchPresenter.getEventById(fav.idEvent)
                    .observeOn(appSchedulerProvider.ui())
                    .subscribeOn(appSchedulerProvider.io())
                    .subscribe {
                        eventList.add(it.events[0])
                        mView.displayFootballMatch(eventList)
                        mView.hideLoading()
                        mView.hideSwipeRefresh()
                    })
        }

        if (favoriteList.isEmpty()) {
            mView.hideLoading()
            mView.displayFootballMatch(eventList)
            mView.hideSwipeRefresh()
        }

    }

    fun destroy() {
        compositeDisposable.dispose()
    }
}