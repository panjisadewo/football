package panji.com.kotlinscheduledicoding.entity.team

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subscribers.ResourceSubscriber
import panji.com.kotlinscheduledicoding.model.teams.TeamPresenter
import panji.com.kotlinscheduledicoding.model.teams.TeamsResponse
import panji.com.kotlinscheduledicoding.utils.SchedulerProviderView
import java.util.*

class TeamPresenter(val mView : TeamView.View, val teamRepositoryImpl: TeamPresenter,
                     val scheduler: SchedulerProviderView): TeamView.Presenter {

    private val compositeDisposable = CompositeDisposable()

    override fun searchTeam(teamName: String) {
//        mView.showLoading()
        compositeDisposable.add(teamRepositoryImpl.getTeamBySearch(teamName)
                .observeOn(scheduler.ui())
                .subscribeOn(scheduler.io())
                .subscribeWith(object: ResourceSubscriber<TeamsResponse>(){
                    override fun onComplete() {
                        mView.hideLoading()
                    }

                    override fun onNext(t: TeamsResponse) {
                        mView.displayTeams(t.teams ?: Collections.emptyList())
                    }

                    override fun onError(t: Throwable?) {
                        mView.displayTeams(Collections.emptyList())
                        mView.hideLoading()
                    }

                })
        )
    }

    override fun getTeamData(leagueName: String) {
        mView.showLoading()
        compositeDisposable.add(teamRepositoryImpl.getAllTeam(leagueName)
                .observeOn(scheduler.ui())
                .subscribeOn(scheduler.io())
                .subscribeWith(object: ResourceSubscriber<TeamsResponse>(){
                    override fun onComplete() {
                        mView.hideLoading()
                    }

                    override fun onNext(t: TeamsResponse) {
                        mView.displayTeams(t.teams ?: Collections.emptyList())
                    }

                    override fun onError(t: Throwable?) {
                        mView.displayTeams(Collections.emptyList())
                        mView.hideLoading()
                    }

                })
        )
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
    }

}