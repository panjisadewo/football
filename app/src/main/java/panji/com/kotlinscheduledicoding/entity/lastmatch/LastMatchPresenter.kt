package panji.com.kotlinscheduledicoding.entity.lastmatch

import panji.com.kotlinscheduledicoding.model.matchevent.MatchEventPresenter
import panji.com.kotlinscheduledicoding.utils.AppSchedulerProvider
import panji.com.kotlinscheduledicoding.utils.SchedulerProviderView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers


class LastMatchPresenter(private val mView : LastMatchView.View,
                         private val matchEventPresenter: MatchEventPresenter,
                         private val appSchedulerProvider: SchedulerProviderView) : LastMatchView.Presenter {

    private val compositeDisposable = CompositeDisposable()

    override fun getFootballData() {
        mView.showLoading()
        compositeDisposable.add(matchEventPresenter.getFootballMatch("4332")
                .observeOn(appSchedulerProvider.ui())
                .subscribeOn(appSchedulerProvider.io())
                .subscribe {
                    mView.displayFootballMatch(it.events)
                    mView.hideLoading()
                }
        )
    }

    fun destroy() {
        compositeDisposable.dispose()
    }
}