package panji.com.kotlinscheduledicoding.entity.playerdetail

import panji.com.kotlinscheduledicoding.model.player.Players

interface PlayerDetailView {

    interface View{
        fun displayPlayerDetail(player: Players)
    }
    interface Presenter{
        fun getPlayerData(idPlayer: String)
        fun onDestroy()
    }
}