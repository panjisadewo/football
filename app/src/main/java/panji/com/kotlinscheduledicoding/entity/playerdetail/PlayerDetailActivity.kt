package panji.com.kotlinscheduledicoding.entity.playerdetail

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_player_detail.*
import kotlinx.android.synthetic.main.item_player_layout_detail.*
import kotlinx.android.synthetic.main.activity_team_detail.*
import panji.com.kotlinscheduledicoding.R
import panji.com.kotlinscheduledicoding.api.TheSportDBApi
import panji.com.kotlinscheduledicoding.api.TheSportDBRest
import panji.com.kotlinscheduledicoding.model.player.PlayerPresenter
import panji.com.kotlinscheduledicoding.model.player.Players
import panji.com.kotlinscheduledicoding.utils.AppSchedulerProvider

class PlayerDetailActivity : AppCompatActivity(), PlayerDetailView.View {

    lateinit var player: Players
    lateinit var mPresenter: PlayerDetailView.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_player_detail)
        setSupportActionBar(toolbar_activity_player_detail)
        player = intent.getParcelableExtra("player")
        supportActionBar?.title = player.strPlayer
        val service = TheSportDBApi.getClient().create(TheSportDBRest::class.java)
        val request = PlayerPresenter(service)
        val scheduler = AppSchedulerProvider()
        mPresenter = PlayerDetailPresenter(this, request, scheduler)
        mPresenter.getPlayerData(player.idPlayer)
    }

    override fun displayPlayerDetail(player: Players) {
        initView(player)
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.onDestroy()
    }

    private fun initView(player: Players){

        loadBanner(player)
        Glide.with(applicationContext)
                .load(player.strCutout)
                .apply(RequestOptions().placeholder(R.drawable.navigation_empty_icon))
                .into(imgPlayer)


        playerName.text = player.strPlayer
        tvPosition.text = player.strPosition
        tvDate.text = player.dateBorn
        playerOverview.text = player.strDescriptionEN
    }

    private fun loadBanner(player: Players){
        if(!player.strFanart1.equals(null)){
            Glide.with(applicationContext)
                    .load(player.strFanart1)
                    .apply(RequestOptions().placeholder(R.drawable.navigation_empty_icon))
                    .into(imageBannerPlayer)
        }else{
            Glide.with(applicationContext)
                    .load(player.strThumb)
                    .apply(RequestOptions().placeholder(R.drawable.navigation_empty_icon))
                    .into(imageBannerPlayer)
        }
    }
}
