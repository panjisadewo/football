package panji.com.kotlinscheduledicoding.entity.playerdetail

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subscribers.ResourceSubscriber
import panji.com.kotlinscheduledicoding.model.player.PlayerDetailResponse
import panji.com.kotlinscheduledicoding.model.player.PlayerPresenter
import panji.com.kotlinscheduledicoding.utils.SchedulerProviderView

class PlayerDetailPresenter(val mView: PlayerDetailView.View,
                            val playersRepositoryImpl: PlayerPresenter,
                            val schedulerProvider: SchedulerProviderView): PlayerDetailView.Presenter {


    val compositeDisposable = CompositeDisposable()

    override fun getPlayerData(idPlayer: String) {
        compositeDisposable.add(playersRepositoryImpl.getPlayerDetail(idPlayer)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribeWith(object: ResourceSubscriber<PlayerDetailResponse>(){
                    override fun onComplete() {

                    }

                    override fun onNext(t: PlayerDetailResponse) {
                        mView.displayPlayerDetail(t.player[0])
                    }

                    override fun onError(t: Throwable?) {

                    }

                })
        )
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
    }
}