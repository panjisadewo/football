package panji.com.kotlinscheduledicoding.entity.searchmatch

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.Menu
import kotlinx.android.synthetic.main.fragment_last_match.*
import panji.com.kotlinscheduledicoding.R
import panji.com.kotlinscheduledicoding.adapter.TeamsAdapter
import panji.com.kotlinscheduledicoding.api.TheSportDBApi
import panji.com.kotlinscheduledicoding.api.TheSportDBRest
import panji.com.kotlinscheduledicoding.model.matchevent.MatchEvent
import panji.com.kotlinscheduledicoding.model.matchevent.MatchEventPresenter
import panji.com.kotlinscheduledicoding.utils.AppSchedulerProvider
import panji.com.kotlinscheduledicoding.utils.hide
import panji.com.kotlinscheduledicoding.utils.show

class SearchMatchActivity : AppCompatActivity(), SearchMatchView.View {

    private var matchLists : MutableList<MatchEvent> = mutableListOf()
    lateinit var mPresenter: SearchMatchView.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_last_match)

        val query = intent.getStringExtra("query")
        Log.v("test", query)
        val service = TheSportDBApi.getClient().create(TheSportDBRest::class.java)
        val request = MatchEventPresenter(service)
        val scheduler = AppSchedulerProvider()
        mPresenter = SearchMatchPresenter(this, request, scheduler)
        mPresenter.searchMatch(query)

    }

    override fun showLoading() {
        mainProgressBarLast.show()
        rvFootballLast.hide()
    }

    override fun hideLoading() {
        mainProgressBarLast.hide()
        rvFootballLast.show()
    }

    override fun displayMatch(matchList: List<MatchEvent>) {
        matchLists.clear()
        matchLists.addAll(matchList)
        val layoutManager = LinearLayoutManager(applicationContext, LinearLayoutManager.VERTICAL, false)
        rvFootballLast.layoutManager = layoutManager
        rvFootballLast.adapter = TeamsAdapter(matchList, applicationContext)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_search, menu)
        val searchView = menu?.findItem(R.id.actionSearch)?.actionView as SearchView?
        searchView?.queryHint = "Search Matches"

        searchView?.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {

                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                mPresenter.searchMatch(newText)
                return false
            }
        })

        return true
    }



}
