package panji.com.kotlinscheduledicoding.entity.nextmatch

import panji.com.kotlinscheduledicoding.model.matchevent.MatchEvent


interface NextMatchView {
    interface View{
        fun hideLoading()
        fun showLoading()
        fun displayFootballMatch(matchList:List<MatchEvent>)
    }

    interface Presenter{
        fun getFootballUpcomingData()

    }
}