package panji.com.kotlinscheduledicoding.entity.teamdetail

import panji.com.kotlinscheduledicoding.model.repo.RepositoryPresenter

class TeamDetailPresenter(val mView: TeamDetailView.View,
                          val localRepositoryImpl: RepositoryPresenter): TeamDetailView.Presenter {


    override fun deleteTeam(id: String) {
        localRepositoryImpl.deleteTeamData(id)
    }

    override fun checkTeam(id: String) {
        mView.setFavoriteState(localRepositoryImpl.checkFavTeam(id))
    }

    override fun insertTeam(id: String, imgUrl: String) {
        localRepositoryImpl.insertTeamData(id, imgUrl)
    }


}