package panji.com.kotlinscheduledicoding.entity.searchmatch

import panji.com.kotlinscheduledicoding.model.matchevent.MatchEvent

interface SearchMatchView {

    interface View{
        fun showLoading()
        fun hideLoading()
        fun displayMatch(matchList: List<MatchEvent>)
    }
    interface Presenter{
        fun searchMatch(query: String?)
        fun onDestroy()
    }

}