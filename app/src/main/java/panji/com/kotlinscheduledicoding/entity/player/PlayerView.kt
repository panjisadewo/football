package panji.com.kotlinscheduledicoding.entity.player

import panji.com.kotlinscheduledicoding.model.player.Players

interface PlayerView {

    interface View{
        fun showLoading()
        fun hideLoading()
        fun displayPlayers(playerList: List<Players>)

    }
    interface Presenter{
        fun getAllPlayer(teamId: String?)
        fun onDestroy()
    }

}