package panji.com.kotlinscheduledicoding.entity.lastmatch

import panji.com.kotlinscheduledicoding.model.matchevent.MatchEvent


interface LastMatchView {
    interface View {
        fun hideLoading()
        fun showLoading()
        fun displayFootballMatch(matchList: List<MatchEvent>)
    }

    interface Presenter {
        fun getFootballData()
    }
}