package panji.com.kotlinscheduledicoding.entity.player

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import panji.com.kotlinscheduledicoding.R
import panji.com.kotlinscheduledicoding.model.player.Players
import kotlinx.android.synthetic.main.fragment_player.*
import panji.com.kotlinscheduledicoding.adapter.PlayersAdapter
import panji.com.kotlinscheduledicoding.api.TheSportDBApi
import panji.com.kotlinscheduledicoding.api.TheSportDBRest
import panji.com.kotlinscheduledicoding.model.player.PlayerPresenter
import panji.com.kotlinscheduledicoding.model.teams.Teams
import panji.com.kotlinscheduledicoding.utils.AppSchedulerProvider
import panji.com.kotlinscheduledicoding.utils.hide
import panji.com.kotlinscheduledicoding.utils.show

class PlayerFragment : Fragment(), PlayerView.View {

    private var listPlayer : MutableList<Players> = mutableListOf()
    lateinit var mPresenter: PlayerView.Presenter

    override fun showLoading() {
        playerProgressbar.show()
        rvPlayer.visibility = View.GONE
    }

    override fun hideLoading() {
        playerProgressbar.hide()
        rvPlayer.visibility = View.VISIBLE
    }

    override fun displayPlayers(playerList: List<Players>) {
        listPlayer.clear()
        listPlayer.addAll(playerList)
        val layoutManager = GridLayoutManager(context, 3)
        rvPlayer.layoutManager = layoutManager
        rvPlayer.adapter = PlayersAdapter(listPlayer, context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_player, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val team: Teams? = arguments?.getParcelable("teams")
        val service = TheSportDBApi.getClient().create(TheSportDBRest::class.java)
        val request = PlayerPresenter(service)
        val scheduler = AppSchedulerProvider()
        mPresenter = panji.com.kotlinscheduledicoding.entity.player.PlayerPresenter(this, request, scheduler)
        mPresenter.getAllPlayer(team?.idTeam)

    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.onDestroy()
    }


}
