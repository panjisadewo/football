package panji.com.kotlinscheduledicoding.entity.lastmatch


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.disposables.CompositeDisposable
import panji.com.kotlinscheduledicoding.R
import panji.com.kotlinscheduledicoding.adapter.TeamsAdapter
import panji.com.kotlinscheduledicoding.api.TheSportDBApi
import panji.com.kotlinscheduledicoding.api.TheSportDBRest
import panji.com.kotlinscheduledicoding.model.matchevent.MatchEvent
import panji.com.kotlinscheduledicoding.model.matchevent.MatchEventPresenter
import panji.com.kotlinscheduledicoding.utils.AppSchedulerProvider
import panji.com.kotlinscheduledicoding.utils.hide
import panji.com.kotlinscheduledicoding.utils.show
import kotlinx.android.synthetic.main.fragment_last_match.*


class LastMatchFragment : Fragment(), LastMatchView.View {
    private lateinit var mPresenter: LastMatchPresenter

    private var matchLists: MutableList<MatchEvent> = mutableListOf()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_last_match, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val service = TheSportDBApi.getClient().create(TheSportDBRest::class.java)
        val request = MatchEventPresenter(service)
        val schedulerProvider = AppSchedulerProvider()
        mPresenter = LastMatchPresenter(this, request, schedulerProvider)
        mPresenter.getFootballData()

    }

    override fun hideLoading() {
        mainProgressBarLast.hide()
        rvFootballLast.visibility = View.VISIBLE
    }

    override fun showLoading() {
        mainProgressBarLast.show()
        rvFootballLast.visibility = View.INVISIBLE
    }

    override fun displayFootballMatch(matchList: List<MatchEvent>) {
        matchLists.clear()
        matchLists.addAll(matchList)
        val layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        rvFootballLast.layoutManager = layoutManager
        rvFootballLast.adapter = TeamsAdapter(matchList, context)
    }

    override fun onDestroyView() {
        mPresenter.destroy()
        super.onDestroyView()
    }
}


