package panji.com.kotlinscheduledicoding.entity.favorite

import panji.com.kotlinscheduledicoding.model.matchevent.MatchEvent

interface FavoriteMatchView {
    interface View {
        fun hideLoading()
        fun showLoading()
        fun displayFootballMatch(matchList: List<MatchEvent>)
        fun hideSwipeRefresh()
        fun onDestroy()
    }

    interface Presenter {
        fun getFootballMatchData()

    }
}