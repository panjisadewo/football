package panji.com.kotlinscheduledicoding.entity.detail

import panji.com.kotlinscheduledicoding.db.FavoriteMatch
import panji.com.kotlinscheduledicoding.model.teams.Teams

interface DetailView {

    interface View{
        fun displayTeamBadgeHome(team: Teams)
        fun setFavoriteState(favList:List<FavoriteMatch>)
        fun displayTeamBadgeAway(team: Teams)
        fun progresBarHide()
        fun progresBarShow()
    }

    interface Presenter{
        fun getTeamsBadgeAway(id:String)
        fun getTeamsBadgeHome(id:String)
        fun deleteMatch(id:String)
        fun insertMatch(eventId: String, homeId: String, awayId: String)
        fun checkMatch(id:String)

    }
}