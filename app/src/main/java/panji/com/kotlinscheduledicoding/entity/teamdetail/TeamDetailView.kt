package panji.com.kotlinscheduledicoding.entity.teamdetail

import panji.com.kotlinscheduledicoding.db.FavoritesTeam

interface TeamDetailView {

    interface View{
        fun setFavoriteState(favList:List<FavoritesTeam>)
    }

    interface Presenter{
        fun deleteTeam(id:String)
        fun checkTeam(id:String)
        fun insertTeam(id: String, imgUrl: String)
    }
}