package panji.com.kotlinscheduledicoding.entity.player

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subscribers.ResourceSubscriber
import panji.com.kotlinscheduledicoding.model.player.PlayerPresenter
import panji.com.kotlinscheduledicoding.model.player.PlayerResponse
import panji.com.kotlinscheduledicoding.utils.SchedulerProviderView
import java.util.*

class PlayerPresenter(val mView: PlayerView.View,
                       val playersRepositoryImpl: PlayerPresenter,
                       val schedulerProvider: SchedulerProviderView): PlayerView.Presenter {

    val compositeDisposable = CompositeDisposable()

    override fun getAllPlayer(teamId: String?) {
        mView.showLoading()
        compositeDisposable.add(playersRepositoryImpl.getAllPlayers(teamId)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribeWith(object: ResourceSubscriber<PlayerResponse>(){
                    override fun onComplete() {
                        mView.hideLoading()
                    }

                    override fun onNext(t: PlayerResponse) {
                        mView.displayPlayers(t.player)
                    }

                    override fun onError(t: Throwable?) {
                        mView.displayPlayers(Collections.emptyList())
                        mView.hideLoading()
                    }

                })
        )
    }
    override fun onDestroy() {
        compositeDisposable.dispose()
    }
}