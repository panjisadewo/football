package panji.com.kotlinscheduledicoding.entity.searchmatch

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subscribers.ResourceSubscriber
import panji.com.kotlinscheduledicoding.model.matchevent.MatchEventView
import panji.com.kotlinscheduledicoding.model.search.SearchMatchResponse
import panji.com.kotlinscheduledicoding.utils.SchedulerProviderView
import java.util.*

class SearchMatchPresenter(val mView: SearchMatchView.View,
                           val matchRepositoryImpl: MatchEventView,
                           val schedulerProvider: SchedulerProviderView): SearchMatchView.Presenter {

    val compositeDisposable = CompositeDisposable()

    override fun searchMatch(query: String?) {
        mView.showLoading()
        compositeDisposable.add(matchRepositoryImpl.searchMatches(query)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribeWith(object: ResourceSubscriber<SearchMatchResponse>(){
                    override fun onComplete() {
                        mView.hideLoading()
                    }

                    override fun onNext(t: SearchMatchResponse) {
                        mView.displayMatch(t.events ?: Collections.emptyList())
                    }

                    override fun onError(t: Throwable?) {
                        mView.displayMatch(Collections.emptyList())
                        mView.hideLoading()
                    }

                })
        )
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
    }


}