package panji.com.kotlinscheduledicoding.model.teams

import com.google.gson.annotations.SerializedName

data class TeamsResponse(
        @SerializedName("teams") var teams: List<Teams>)