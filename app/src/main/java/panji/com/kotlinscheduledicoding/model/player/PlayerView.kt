package panji.com.kotlinscheduledicoding.model.player

import io.reactivex.Flowable

interface PlayerView {

    fun getAllPlayers(teamId: String?) : Flowable<PlayerResponse>
    fun getPlayerDetail(playerId: String?) : Flowable<PlayerDetailResponse>
}