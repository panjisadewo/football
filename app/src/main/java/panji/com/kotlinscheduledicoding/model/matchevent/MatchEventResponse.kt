package panji.com.kotlinscheduledicoding.model.matchevent

import com.google.gson.annotations.SerializedName
import panji.com.kotlinscheduledicoding.model.matchevent.MatchEvent

data class MatchEventResponse (
        @SerializedName("events") var events: List<MatchEvent>
)
