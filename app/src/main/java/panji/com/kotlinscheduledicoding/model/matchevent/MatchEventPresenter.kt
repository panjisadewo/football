package panji.com.kotlinscheduledicoding.model.matchevent


import panji.com.kotlinscheduledicoding.api.TheSportDBRest
import panji.com.kotlinscheduledicoding.model.teams.TeamsResponse
import io.reactivex.Flowable


class MatchEventPresenter(private val theSportDBRest: TheSportDBRest) : MatchEventView {

    override fun searchMatches(query: String?) = theSportDBRest.searchMatches(query)

    override fun getUpcomingMatch(id: String): Flowable<MatchEventResponse> = theSportDBRest.getUpcomingMatch(id)

    override fun getFootballMatch(id: String): Flowable<MatchEventResponse> = theSportDBRest.getLastmatch(id)

    override fun getTeams(id: String): Flowable<TeamsResponse> = theSportDBRest.getTeam(id)

    fun getEventById(id: String): Flowable<MatchEventResponse> = theSportDBRest.getEventById(id)
}