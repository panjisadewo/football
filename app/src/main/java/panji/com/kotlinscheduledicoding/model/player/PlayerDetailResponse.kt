package panji.com.kotlinscheduledicoding.model.player

import com.google.gson.annotations.SerializedName

data class PlayerDetailResponse(
        @SerializedName("player") var player: List<Players>)