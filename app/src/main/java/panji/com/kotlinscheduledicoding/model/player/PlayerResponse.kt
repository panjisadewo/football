package panji.com.kotlinscheduledicoding.model.player

import com.google.gson.annotations.SerializedName

data class PlayerResponse(
        @SerializedName("player") var player: List<Players>)

