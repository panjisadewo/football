package panji.com.kotlinscheduledicoding.model.search

import com.google.gson.annotations.SerializedName
import panji.com.kotlinscheduledicoding.model.matchevent.MatchEvent

data class SearchMatchResponse(
        @SerializedName("event") var events: List<MatchEvent>
)