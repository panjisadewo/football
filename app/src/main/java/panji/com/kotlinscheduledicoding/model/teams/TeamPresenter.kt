package panji.com.kotlinscheduledicoding.model.teams

import io.reactivex.Flowable
import panji.com.kotlinscheduledicoding.api.TheSportDBRest

class TeamPresenter(val teamRest: TheSportDBRest) : TeamView{

    override fun getAllTeam(id: String) = teamRest.getAllTeam(id)
    override fun getTeamBySearch(query: String) = teamRest.getTeamBySearch(query)
    override fun getTeams(id: String): Flowable<TeamsResponse> = teamRest.getAllTeam(id)
    override fun getTeamsDetail(id: String): Flowable<TeamsResponse> = teamRest.getTeam(id)

}