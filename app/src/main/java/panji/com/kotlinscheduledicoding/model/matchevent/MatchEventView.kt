package panji.com.kotlinscheduledicoding.model.matchevent

import panji.com.kotlinscheduledicoding.model.teams.TeamsResponse
import io.reactivex.Flowable
import panji.com.kotlinscheduledicoding.model.search.SearchMatchResponse

interface MatchEventView {

    fun getFootballMatch(id : String) : Flowable<MatchEventResponse>

    fun getTeams(id : String = "0") : Flowable<TeamsResponse>

    fun getUpcomingMatch(id : String) : Flowable<MatchEventResponse>

    fun searchMatches(query: String?) : Flowable<SearchMatchResponse>

}