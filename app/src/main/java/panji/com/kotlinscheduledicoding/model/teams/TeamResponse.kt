package panji.com.kotlinscheduledicoding.model.teams

import com.google.gson.annotations.SerializedName

data class TeamResponse(
        @SerializedName("teams") var teams: List<Teams>)