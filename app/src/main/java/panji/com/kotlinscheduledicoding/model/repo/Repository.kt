package panji.com.kotlinscheduledicoding.model.repo

import panji.com.kotlinscheduledicoding.db.FavoriteMatch
import panji.com.kotlinscheduledicoding.db.FavoritesTeam

interface Repository {

    fun getMatchFromDb() : List<FavoriteMatch>

    fun insertData(eventId: String, homeId: String, awayId: String)

    fun deleteData(eventId: String)

    fun checkFavorite(eventId: String) : List<FavoriteMatch>

    fun getTeamFromDb() : List<FavoritesTeam>

    fun insertTeamData(teamId: String, imgUrl: String)

    fun deleteTeamData(teamId: String)

    fun checkFavTeam(teamId: String) : List<FavoritesTeam>
}