package panji.com.kotlinscheduledicoding.model.repo

import android.content.Context
import panji.com.kotlinscheduledicoding.db.FavoriteMatch
import panji.com.kotlinscheduledicoding.db.database
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import panji.com.kotlinscheduledicoding.db.FavoritesTeam

class RepositoryPresenter(private val context: Context) : Repository {

    override fun checkFavorite(eventId: String): List<FavoriteMatch> {
        return context.database.use {
            val result = select(FavoriteMatch.TABLE_FAVORITE)
                    .whereArgs("(EVENT_ID = {id})",
                            "id" to eventId)
            val favorite = result.parseList(classParser<FavoriteMatch>())
            favorite
        }
    }


    override fun deleteData(eventId: String) {
        context.database.use{
            delete(FavoriteMatch.TABLE_FAVORITE, "(EVENT_ID = {id})",
                    "id" to eventId)
        }
    }

    override fun insertData(eventId: String, homeId: String, awayId: String) {
        context.database.use {
            insert(FavoriteMatch.TABLE_FAVORITE,
                    FavoriteMatch.EVENT_ID to eventId,
                    FavoriteMatch.HOME_TEAM_ID to homeId,
                    FavoriteMatch.AWAY_TEAM_ID to awayId)

        }
    }

    override fun getMatchFromDb(): List<FavoriteMatch> {
        lateinit var favoriteList :List<FavoriteMatch>
        context.database.use {
            val result = select(FavoriteMatch.TABLE_FAVORITE)
            val favorite = result.parseList(classParser<FavoriteMatch>())
            favoriteList = favorite
        }
        return favoriteList
    }

    override fun getTeamFromDb(): List<FavoritesTeam> {
        lateinit var favoriteList :List<FavoritesTeam>
        context.database.use {
            val result = select(FavoritesTeam.TEAM_TABLE)
            val favorite = result.parseList(classParser<FavoritesTeam>())
            favoriteList = favorite
        }
        return favoriteList
    }

    override fun insertTeamData(teamId: String, imgUrl: String) {
        context.database.use {
            insert(FavoritesTeam.TEAM_TABLE,
                    FavoritesTeam.TEAM_ID to teamId,
                    FavoritesTeam.TEAM_IMAGE to imgUrl)

        }

    }
    override fun deleteTeamData(teamId: String) {
        context.database.use{
            delete(FavoritesTeam.TEAM_TABLE, "(TEAM_ID = {id})",
                    "id" to teamId)
        }
    }

    override fun checkFavTeam(teamId: String): List<FavoritesTeam> {
        return context.database.use {
            val result = select(FavoritesTeam.TEAM_TABLE)
                    .whereArgs("(TEAM_ID = {id})",
                            "id" to teamId)
            val favorite = result.parseList(classParser<FavoritesTeam>())
            favorite
        }
    }
}