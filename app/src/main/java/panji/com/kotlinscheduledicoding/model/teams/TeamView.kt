package panji.com.kotlinscheduledicoding.model.teams

import io.reactivex.Flowable

interface TeamView {

    fun getTeams(id : String = "0") : Flowable<TeamsResponse>

    fun getTeamsDetail(id : String = "0") : Flowable<TeamsResponse>

    fun getTeamBySearch(query: String) : Flowable<Teams>

    fun getAllTeam(id: String) : Flowable<TeamsResponse>
}