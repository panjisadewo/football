package panji.com.kotlinscheduledicoding.model.player

import panji.com.kotlinscheduledicoding.api.TheSportDBRest

class PlayerPresenter(private val footballRest: TheSportDBRest): PlayerView {

    override fun getAllPlayers(teamId: String?) = footballRest.getAllPlayers(teamId)

    override fun getPlayerDetail(playerId: String?) = footballRest.getPlayerDetail(playerId)
}