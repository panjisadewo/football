package panji.com.kotlinscheduledicoding.api

import panji.com.kotlinscheduledicoding.BuildConfig
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

import java.net.URL

class TheSportDBApi {
    fun doRequest(url: String): String {
        return URL(url).readText()
    }

    companion object {
        fun getClient(): Retrofit {
            return Retrofit.Builder()
                    .baseUrl(BuildConfig.BASE_URL +
                            "api/v1/json/${BuildConfig.TSDB_API_KEY}")
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()
        }
    }
}