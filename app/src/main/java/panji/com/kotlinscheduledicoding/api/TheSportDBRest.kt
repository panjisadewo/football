package panji.com.kotlinscheduledicoding.api

import panji.com.kotlinscheduledicoding.model.matchevent.MatchEventResponse
import panji.com.kotlinscheduledicoding.model.teams.TeamsResponse
import panji.com.kotlinscheduledicoding.model.search.SearchMatchResponse
import io.reactivex.Flowable
import panji.com.kotlinscheduledicoding.model.player.PlayerDetailResponse
import panji.com.kotlinscheduledicoding.model.player.PlayerResponse
import panji.com.kotlinscheduledicoding.model.teams.Teams
import retrofit2.http.GET
import retrofit2.http.Query

interface TheSportDBRest {

    @GET("eventspastleague.php")
    fun getLastmatch(@Query("id") id:String) : Flowable<MatchEventResponse>

    @GET("eventsnextleague.php")
    fun getUpcomingMatch(@Query("id") id:String) : Flowable<MatchEventResponse>

    @GET("lookupteam.php")
    fun getTeam(@Query("id") id:String) : Flowable<TeamsResponse>


    @GET("lookupevent.php")
    fun getEventById(@Query("id") id:String) : Flowable<MatchEventResponse>

    @GET("searchevents.php")
    fun searchMatches(@Query("e") query: String?) : Flowable<SearchMatchResponse>

    @GET("searchteams.php")
    fun getTeamBySearch(@Query("t") query: String) : Flowable<Teams>

    @GET("lookup_all_teams.php")
    fun getAllTeam(@Query("id") id:String) : Flowable<TeamsResponse>

    @GET("lookup_all_players.php")
    fun getAllPlayers(@Query("id") id:String?) : Flowable<PlayerResponse>

    @GET("lookupplayer.php")
    fun getPlayerDetail(@Query("id") id:String?) : Flowable<PlayerDetailResponse>
}