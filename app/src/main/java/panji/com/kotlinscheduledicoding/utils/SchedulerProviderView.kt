package panji.com.kotlinscheduledicoding.utils

import io.reactivex.Scheduler


interface SchedulerProviderView {
    fun ui(): Scheduler
    fun io(): Scheduler

}