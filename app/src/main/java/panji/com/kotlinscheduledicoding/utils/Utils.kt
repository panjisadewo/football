package panji.com.kotlinscheduledicoding.utils

import android.view.View


fun View.hide() {
    visibility = View.GONE
}

fun View.show() {
    visibility = View.VISIBLE
    }