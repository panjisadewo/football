package panji.com.kotlinscheduledicoding

import android.support.test.espresso.Espresso.onView
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import panji.com.kotlinscheduledicoding.entity.main.MainActivity
import android.support.test.espresso.Espresso.pressBack
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.v7.widget.RecyclerView

import panji.com.kotlinscheduledicoding.R.id.*


@RunWith(AndroidJUnit4::class)
class MainTest {
    @Rule
    @JvmField
    var activityRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun testAppBehaviour() {

        delay()

        onView(withId(lastMatch))
                .check(matches(isDisplayed()))
        delay()

        onView(withId(lastMatch)).perform(click())

        delay()

        onView(withId(nextMatch))
                .check(matches(isDisplayed()))

        delay()

        onView(withId(nextMatch)).perform(click())

        delay()

        onView(withId(favMatch))
                .check(matches(isDisplayed()))

        delay()

        onView(withId(favMatch)).perform(click())

        delay()

    }

    private fun delay(){
        try {
            Thread.sleep(5000)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
    }
}


